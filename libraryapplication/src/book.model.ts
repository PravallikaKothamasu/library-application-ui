export class Book{
    bookId: number;
    title: string;
    author: string;
    genre: string;
    cost: number;

}