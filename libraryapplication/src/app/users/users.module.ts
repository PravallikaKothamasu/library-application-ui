import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserlistComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [UserlistComponent]
})
export class UsersModule { }
