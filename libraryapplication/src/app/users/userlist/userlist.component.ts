import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { User } from 'src/user.model';
import { FormsModule } from '@angular/forms';
import { UserDTO } from 'src/app/user-dto';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private userService: UserService) { }

  userList: User;
  isAdd: boolean = false;
  isEdit: boolean = false;

  userModel = new UserDTO('','');
  selectedUserId: number;
  selectedUser: User;

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers(){
    this.userService.getUsers("users").subscribe( (res:User) => this.userList = res);
    console.log(this.userList);
  }

  addUser(){
    this.isAdd = true;
    this.isEdit = false;
    this.userModel = new UserDTO('','');
    console.log('inside add user action');
  }

  updateUser(user: User){
    this.isEdit = true;
    this.isAdd = false;
    console.log('inside update user action');
    this.selectedUser = user;
    this.map(user);
    this.selectedUserId = user.userId;
  }

  map(user){
    this.userModel.userName = user.userName;
    this.userModel.password = user.password; 
  }

  issueBook(){
    console.log('inside issue book');
  }

  onSubmit(){
    if(this.isAdd){
      console.log('add user operation to be performed');
      console.log(this.userModel);
      this.userService.addUser("users",this.userModel).subscribe( res=> {this.ngOnInit();} );
      console.log('will be added');
    }if(this.isEdit){
      console.log('update user action to be performed');
      this.userService.updateUser("users",this.selectedUserId,this.userModel).subscribe( res=> {this.ngOnInit(); });
      console.log(this.userModel);
      console.log('is updated');
    }
    document.getElementById('close').click(); 
  }

  deleteUser(user:User){
    console.log('inside delete');
    console.log(user.userId);
    this.userService.deleteUserById("users", user.userId).subscribe( res=> {this.ngOnInit();});
  }

}
