import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksModule } from './books/books.module';
import { UsersModule } from './users/users.module';
import { IssueinformationModule } from './issueinformation/issueinformation.module';
import { BookService } from './book.service';
import { HttpClientModule } from '@angular/common/http';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BooksModule,
    UsersModule,
    IssueinformationModule,
    HttpClientModule
  ],
  providers: [BookService], 
  bootstrap: [AppComponent] 
})
export class AppModule { }
