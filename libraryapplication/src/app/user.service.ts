import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from './../environments/environment';
import { User } from 'src/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl: string;
  users: User;

  constructor(private http: HttpClient) { 
    this.baseUrl = environment.domainUrl;
  }

  getUsers(endpoint){
    return this.http.get(this.baseUrl + endpoint);
  }

  addUser(endpoint,body){
    return this.http.post(this.baseUrl + endpoint,body);
  }

  updateUser(endpoint,userId,body){
    return this.http.put(this.baseUrl + endpoint + "/" + userId,body);
  }

  deleteUserById(endpoint,userId){
    return this.http.delete(this.baseUrl + endpoint + "/" + userId);
  }

}
