import { Component, OnInit, Output, EventEmitter,Input } from '@angular/core';
import { BookService } from 'src/app/book.service';
import { Router } from '@angular/router'
import { Book } from 'src/book.model';
import {FormsModule} from '@angular/forms';
import { BookDTO } from 'src/app/book-dto';
import { User } from 'src/user.model';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.css']
})
export class BooklistComponent implements OnInit {

  constructor(private bookService: BookService, private router: Router) { }

  bookList: Book;
  bookModel = new BookDTO('','','',0);
  toAdd = false;
  toUpdate = false;
  selectedBook: BookDTO;
  selectedBookId: number;


  ngOnInit(): void {
    this.getAllBooks();
  }

  getAllBooks(){
    this.bookService.getBooks("books").subscribe( (res :Book) => this.bookList = res);
  }

  deleteBook(book: Book){
    this.bookService.deleteBookById("books",book.bookId).subscribe(res =>{   this.ngOnInit(); });
    console.log("[" +book.bookId + ":" + book.author + "]" + "  Deleted successfully");
  }

  addBookAction(){
    console.log('clicked add button');
    this.bookModel = new BookDTO('','','',0);
    this.toAdd = true;
    this.toUpdate = false;
  }

  updateBookAction(book:Book){
    console.log('clicked update button');
    this.toUpdate = true;
    this.toAdd = false;
    this.selectedBook = book;
    this.selectedBookId = book.bookId;
    this.map();
  }

  map(){
    this.bookModel.title = this.selectedBook.title;
    this.bookModel.author=this.selectedBook.author;
    this.bookModel.genre=this.selectedBook.genre;
    this.bookModel.cost=this.selectedBook.cost;
  }


  onSubmit(){
    if(this.toAdd){
    this.bookService.addBook("books",this.bookModel).subscribe(res =>{   this.ngOnInit(); });
    console.log(this.bookModel);
    console.log('is added');
    }else if(this.toUpdate){
      console.log('edit operation to be performed');
      this.bookService.updateBook("books", this.selectedBookId, this.bookModel).subscribe(res =>{ this.ngOnInit(); });
      console.log(this.bookModel);
      console.log('is updated');
    }else{
      console.log('invalid operation');
    }
    document.getElementById("close").click();
  }

}
