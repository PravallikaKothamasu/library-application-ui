import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooklistComponent } from './booklist/booklist.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [BooklistComponent, BookDetailComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BooklistComponent]
})
export class BooksModule { }
