import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/book.service';
import { Book } from 'src/book.model';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  public bookId: number;
  public selectedBook: Book;

  constructor(private selectedRoute: ActivatedRoute, private bookService: BookService) { }

  ngOnInit(): void {
    //let id = parseInt(this.selectedRoute.snapshot.paramMap.get('bookId'));
    //this.bookId = id;
    this.getSelectedBookById();
  }


  getSelectedBookById(){
      this.bookService.getBookById("books",this.bookId).subscribe( (res: Book) => this.selectedBook = res);
  }

}
