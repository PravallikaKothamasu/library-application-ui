import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { User } from 'src/user.model';
import { IssueBook } from 'src/app/IssueBook.model';
import { IssueService } from 'src/app/issue.service';
import { IssuebookDTO } from 'src/app/issuebook-dto';
import { error } from 'protractor';


@Component({
  selector: 'app-issuelist',
  templateUrl: './issuelist.component.html',
  styleUrls: ['./issuelist.component.css']
})
export class IssuelistComponent implements OnInit {

  userList: User;
  booksIssued = [];
  message: string;
  isEmpty: boolean;
  userIdToIssue: number;
  public bookIdToIssue: number;
  bookToBeIssued = new IssuebookDTO(0,0);
  messageToDisplay: string;


  constructor(private userService: UserService, private issueService: IssueService) {
   
   }

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers(){
    this.userService.getUsers("users").subscribe( (res:User) => this.userList = res);
  }


  getIssuedBooksOfUser(userId){
    console.log('inside getIssuedBooksOsUser of: '+ userId);
    this.issueService.getIssuedBooksOfUser("users",userId)
    .subscribe( res => {
      this.booksIssued = res;
    },
    ()=>{},
    () => {
      if(this.booksIssued.length == 0){
      this.message = "No records found";
      this.isEmpty = true; 
      } else{
        this.message = "";
        this.isEmpty = false;
      }
    }); 
  }

  releaseBook(bookToBeReleased: IssueBook){
    console.log('inside release book, userId: '+bookToBeReleased.userId+' and bookId: '+bookToBeReleased.bookId);
    this.issueService.releaseIssuedBookForUser(bookToBeReleased.userId, bookToBeReleased.bookId)
    .subscribe( res => this.getIssuedBooksOfUser(bookToBeReleased.userId) );
    }

    getUserIdToIssue(userId){
      this.userIdToIssue = userId;
      this.messageToDisplay="";
    }

    whenClicked(){
      console.log('inside clicked event');
      this.messageToDisplay = "";
    }

    issueBook(){
      console.log('inside issueBook with user Id: '+this.userIdToIssue + ' and bookId to issue: '+this.bookIdToIssue);
      this.bookToBeIssued.bookIdToIssue = this.bookIdToIssue;
      this.bookToBeIssued.userIdToIssue = this.userIdToIssue;
      console.log(this.bookToBeIssued);
      this.issueService.issueBookToUser(this.userIdToIssue,this.bookIdToIssue,this.bookToBeIssued)
      .subscribe( res => {
        this.ngOnInit(); 
        this.messageToDisplay = "Book is successfully issued";
      },
      (err) => {
         console.log('error code: '+err.status);
         if(err.status == 404){
          this.messageToDisplay = "No book found";
         }else if(err.status == 400){
           this.messageToDisplay = "Book is already issued to user";
         }
    },
      () => {}
      );
     this.bookIdToIssue = parseInt("");
     this.messageToDisplay="";
    }

}
