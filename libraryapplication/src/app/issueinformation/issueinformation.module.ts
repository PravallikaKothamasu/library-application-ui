import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuelistComponent } from './issuelist/issuelist.component';
import {  FormsModule } from '@angular/forms'


@NgModule({
  declarations: [IssuelistComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [IssuelistComponent]
})
export class IssueinformationModule { }
