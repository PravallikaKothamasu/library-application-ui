import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooklistComponent } from './books/booklist/booklist.component';
import { UserlistComponent } from './users/userlist/userlist.component';
import { IssuelistComponent } from './issueinformation/issuelist/issuelist.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BookDetailComponent } from './books/book-detail/book-detail.component';


const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'books', component: BooklistComponent},
  {path: 'books/:bookId', component: BookDetailComponent},
  {path: 'users', component: UserlistComponent},
  {path: 'issues', component: IssuelistComponent},
  {path: "**", component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
