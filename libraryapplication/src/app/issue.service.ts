import { Injectable } from '@angular/core';
import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http'
import { Observable, throwError  } from 'rxjs';
import { IssueBook } from './IssueBook.model';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  baseUrl: string;
  

  constructor(private http: HttpClient) {
    this.baseUrl = environment.domainUrl;
   }

   getIssuedBooksOfUser(endpoint,userId): Observable<IssueBook[]>{
      return this.http.get<IssueBook[]>(this.baseUrl + endpoint + "/" + userId +"/issuedbooks");
   }

   releaseIssuedBookForUser(userId,bookId){
      return this.http.delete(this.baseUrl + "users/" + userId + "/books/" + bookId);
   }

   issueBookToUser(userId, bookId, body){
    return this.http.post(this.baseUrl + "users/" + userId + "/books/" + bookId,body)
   }


}
