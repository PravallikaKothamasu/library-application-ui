export class BookDTO {
    constructor(
        public title: string,
        public author: string,
        public genre: string,
        public cost: number
    ){

    }
}
