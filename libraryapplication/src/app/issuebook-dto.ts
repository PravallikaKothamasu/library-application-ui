export class IssuebookDTO {
    constructor(
        public bookIdToIssue: number,
        public userIdToIssue: number
    ){

    }
}
