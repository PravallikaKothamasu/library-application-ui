import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';
import { Book } from 'src/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  baseUrl: string;

  constructor(private http : HttpClient) { 
    this.baseUrl = environment.domainUrl;
  }

  getBooks(endpoint){
    return this.http.get(this.baseUrl + endpoint);
  }

  getBookById(endpoint, bookId){
    return this.http.get(this.baseUrl + endpoint + "/" + bookId);
  }

  deleteBookById(endpoint, bookId){
    return this.http.delete(this.baseUrl + endpoint + "/" + bookId);
  }

  addBook(endpoint,body){
    return this.http.post(this.baseUrl + endpoint , body);
  }

  updateBook(endpoint,bookId,body){
    console.log('inside put method')
    console.log(bookId);
    return this.http.put(this.baseUrl + endpoint + "/" + bookId, body);
  }

}
